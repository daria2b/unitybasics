using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//can be attached to a prefab to be instantiated
//allows to set a variable in inspector to destroy object after X seconds after instantiation
public class DestroyObjectInNSeconds : MonoBehaviour {

	public float aliveTime;

	// Use this for initialization
	void Awake () {
		Destroy (gameObject, aliveTime);
	}

}